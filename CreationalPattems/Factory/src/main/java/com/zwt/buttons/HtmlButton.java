package com.zwt.buttons;
/**
 * @author ML李嘉图
 * @version createtime: 2021-11-10
 * Blog: https://www.cnblogs.com/zwtblog/
 */
/**
 * HTML button implementation.
 */
public class HtmlButton implements Button {

    public void render() {
        System.out.println("<button>Test Button</button>");
        onClick();
    }

    public void onClick() {
        System.out.println("Click! Button says - 'Hello World!'");
    }
}