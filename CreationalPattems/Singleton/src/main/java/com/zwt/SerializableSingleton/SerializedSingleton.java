package com.zwt.SerializableSingleton;

import java.io.Serializable;

/**
 * @author ML李嘉图
 * @version createtime: 2021-11-09
 * Blog: https://www.cnblogs.com/zwtblog/
 */
public class SerializedSingleton implements Serializable {

    private static final long serialVersionUID = -7604766932017737115L;

    private SerializedSingleton() {
    }

    private static class SingletonHelper {
        private static final SerializedSingleton instance = new SerializedSingleton();
    }

    public static SerializedSingleton getInstance() {
        return SingletonHelper.instance;
    }

    //这样当JVM从内存中反序列化地"组装"一个新对象时
    //就会自动调用这个 readResolve方法来返回我们指定好的对象了,
    protected Object readResolve() {
        return getInstance();
    }

}
