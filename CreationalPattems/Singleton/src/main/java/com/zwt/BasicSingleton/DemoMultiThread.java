package com.zwt.BasicSingleton;
/**
 * @author ML李嘉图
 * @version createtime: 2021-11-09
 * Blog: https://www.cnblogs.com/zwtblog/
 */
public class DemoMultiThread {
    public static void main(String[] args) {
        System.out.println("If you see the same value, then singleton was reused (yay!)" + "\n" +
                "If you see different values, then 2 singletons were created (booo!!)" + "\n\n" +
                "RESULT:" + "\n");

        Thread threadFoo = new Thread(new Threadwb());
        Thread threadBar = new Thread(new Threadzwt());
        threadFoo.start();
        threadBar.start();

    }

    static class Threadwb implements Runnable {
        @Override
        public void run() {
            Singleton singleton = Singleton.getInstance("wb");
            System.out.println(singleton.value);
        }
    }

    static class Threadzwt implements Runnable {
        @Override
        public void run() {
            Singleton singleton = Singleton.getInstance("zwt");
            System.out.println(singleton.value);
        }
    }
}
