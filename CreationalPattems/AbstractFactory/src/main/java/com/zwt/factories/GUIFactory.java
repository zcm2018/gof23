package com.zwt.factories;

/**
 * @author ML李嘉图
 * @version createtime: 2021-11-10
 * Blog: https://www.cnblogs.com/zwtblog/
 */

import com.zwt.buttons.Button;
import com.zwt.checkboxes.Checkbox;

/**
 * Abstract factory knows about all (abstract) product types.
 */

public interface GUIFactory {
    Button createButton();

    Checkbox createCheckbox();
}
