package com.zwt.commands;
/**
 * @author ML李嘉图
 * @version createtime: 2022-01-13
 * Blog: https://www.cnblogs.com/zwtblog/
 */
//time: 2022/1/13
import com.zwt.editor.Editor;

public abstract class Command {
    public Editor editor;
    private String backup;

    Command(Editor editor) {
        this.editor = editor;
    }

    void backup() {
        backup = editor.textField.getText();
    }

    public void undo() {
        editor.textField.setText(backup);
    }

    public abstract boolean execute();
}
