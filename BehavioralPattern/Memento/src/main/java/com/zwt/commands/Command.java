package com.zwt.commands;

public interface Command {
    String getName();

    void execute();
}